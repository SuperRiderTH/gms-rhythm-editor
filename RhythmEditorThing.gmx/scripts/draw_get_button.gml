//A horribly cannibalized script from GMLscripts.com for mouse buttons.

{
    var xx,yy,str,w,h,col1,col2,col3,prev_color,prev_alpha,bl,br,bt,bb,in;
    xx = argument0;
    yy = argument1;
    str = argument2;
    if argument3 == 0{
        w = string_width(argument2);
    } else {
        w = argument3
        global.objIntW = w
    }
    h = string_height(argument2)*1.5;

    bl = xx ;
    br = xx + w + 10;
    bt = yy ;
    bb = yy + h;
    
    in = (mouse_x>bl && mouse_x<br && mouse_y>bt && mouse_y<bb);

    if (in) {
        if THEME_HAS_HOVER {
            if mouse_check_button(mb_left) {
                draw_rectangle_colour(bl,bt,br,bb,C_SELECT,C_SELECT,C_SELECT,C_SELECT,0)
            } else {
                draw_rectangle_colour(bl,bt,br,bb,C_HOVER,C_HOVER,C_HOVER,C_HOVER,0)
            }
        }
        if THEME_HAS_OUTLINES {
            draw_rectangle_colour(bl,bt,br,bb,C_OUTLINE,C_OUTLINE,C_OUTLINE,C_OUTLINE,1)
        }
        draw_set_color(C_FONT);
        if mouse_check_button_released(mb_left) {
            return true;
        }
    }
        draw_set_color(C_FONT);
        draw_set_halign(fa_center);
        draw_set_valign(fa_middle);
        draw_text(bl+((w/2)+5),bt+(h/2),str);
        draw_set_halign(fa_left);
        draw_set_valign(fa_top);
    //draw_set_color(prev_color);
    return false;
}
