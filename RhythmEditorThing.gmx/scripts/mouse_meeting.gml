//mouse_meeting(x,y,x2,y2,Draw a rectangle on collision.,Draw the rectangle as an outline.)
//Function used for checking if the mouse is in certain coordinates
//TODO: Make mouse_meeting_debug for drawing the rectangle.

var outline;

if argument5 == 1
{
    outline = 1
}
else
{
    outline = 0
}

if  ( mouse_x>=argument0 && mouse_x<=argument2 )
and ( mouse_y>=argument1 && mouse_y<=argument3 )
then 
{
    if argument4 == 1
    {
        draw_rectangle(argument0,argument1,argument2,argument3,outline)
    }
    return 1
    window_set_cursor(cr_handpoint)
}
else 
{
    return 0
    window_set_cursor(cr_default)
}
