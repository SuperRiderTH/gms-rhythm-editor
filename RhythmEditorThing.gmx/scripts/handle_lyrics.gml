var x1,y1,x2,y2;

y1 = objRhythmEditor.surface_video_height+2+objRhythmEditor.timeline_height[1]+2;
y2 = objRhythmEditor.surface_video_height+objRhythmEditor.timeline_height[2]-2;

with(all) {
    if object_index == objLyric {
        x1 = x+objRhythmEditor.timeline_offset;
        x2 = x1+length;
        
        if mouse_meeting(x1,y1,x2,y2,0,0) {
            if mouse_check_button_pressed(1){
                if objRhythmEditor.music_pos > x {
                    length = objRhythmEditor.music_pos-x
                }
            }
                if mouse_check_button_pressed(2){
                    instance_destroy()
            }
        } 
    }
}
