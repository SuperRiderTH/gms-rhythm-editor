globalvar C_BACK,C_SUBMENU,C_OUTLINE,C_FONT,C_HOVER,C_SELECT,C_PROGRESS,FONT_SYSTEM;
globalvar THEME_HAS_OUTLINES,THEME_HAS_HOVER,THEME_HAS_OUTLINES;

if os_type == os_windows {
    FONT_SYSTEM         = font_SegoeUI;
    C_BAR               = $ffffff;
    C_BACK              = $ffffff;
    C_FONT              = $000000;
    C_SUBMENU           = $f7f6f5;
    C_OUTLINE           = $dcdbda;
    C_HOVER             = $fff3e5;
    C_SELECT            = $ffe8cc;
    C_PROGRESS          = $25b006;
    THEME_HAS_OUTLINES  = true;
    THEME_HAS_SHADOWS   = false;
    THEME_HAS_HOVER     = true;
    THEME_HAS_HEADER    = true;
} else
if os_type == os_android {
    FONT_SYSTEM         = font_Roboto;
    C_BAR               = $7E231A;
    C_BACK              = $FAFAFA;
    C_FONT              = $FFFFFF;
    C_SUBMENU           = $933528;
    C_OUTLINE           = $dcdbda;
    C_HOVER             = C_BACK;
    C_SELECT            = C_BACK;
    C_PROGRESS          = $D4BC00;
    THEME_HAS_OUTLINES  = false;
    THEME_HAS_SHADOWS   = true;
    THEME_HAS_HOVER     = false;
    THEME_HAS_HEADER    = false;
} else 
{
    FONT_SYSTEM         = font_Roboto;
    C_BAR               = $7E231A;
    C_BACK              = $FAFAFA;
    C_FONT              = $FFFFFF;
    C_SUBMENU           = $933528;
    C_OUTLINE           = $dcdbda;
    C_HOVER             = C_BACK;
    C_SELECT            = C_BACK;
    C_PROGRESS          = $D4BC00;
    THEME_HAS_OUTLINES  = false;
    THEME_HAS_SHADOWS   = true;
    THEME_HAS_HOVER     = false;
    THEME_HAS_HEADER    = false;
}

