//A horribly cannibalized script from GMLscripts.com for mouse buttons.
//Now with pictures.
//TODO: Rewrite. Most, if not all, of this is not needed.

{
    var xx,yy,str,w,h,col1,col2,col3,prev_color,prev_alpha,bl,br,bt,bb,in;
    xx = argument0;
    yy = argument1;
    spr = argument2;
    str = argument3
    w = sprite_get_width(spr)
    if w<64
    {
        w = 64
    }
    h = 64 ;
    col1 = c_black;
    col2 = c_black;
    col3 = global.guicolor;
    prev_color = draw_get_color();
    prev_alpha = draw_get_alpha();
    bl = xx ;
    br = xx + w + 10;
    bt = yy ;
    bb = yy + h;
    in = (mouse_x>bl && mouse_x<br && mouse_y>bt && mouse_y<bb);
    
    if (in) {
        draw_set_color(col3);
        draw_set_alpha(0.2);
        draw_rectangle(bl,bt,br,bb,0);
        draw_set_color(col2);
        draw_rectangle(bl,bt,br,bb,1);
        draw_set_alpha(1);
    }else{
        draw_set_color(col1);
        draw_set_alpha(1);
    }
    //draw_text(xx+5+sprite_get_width(spr),yy,str)
    if sprite_get_xoffset(spr)!=0
    {
        xx = xx+sprite_get_xoffset(spr)
    }
    if sprite_get_yoffset(spr)!=0
    {
        yy = yy+sprite_get_yoffset(spr)
    }
    draw_sprite(spr,0,xx+5,yy);
    
    draw_set_color(prev_color);
    draw_set_alpha(prev_alpha);
    if (in && mouse_check_button_pressed(mb_left)) return true;
    return false;
}
