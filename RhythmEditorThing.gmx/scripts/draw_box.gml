//draw_box(x1,y1,x2,y2,color)

var x1,y1,x2,y2;
x1 = floor(argument[0]);
y1 = floor(argument[1]);
x2 = floor(argument[2]);
y2 = floor(argument[3]);

if argument_count < 6 {
    draw_rectangle_colour(x1,y1,x2,y2,argument[4],argument[4],argument[4],argument[4],0);
}

if argument_count == 6 {
    if argument[5] == true {
        draw_rectangle_colour(x1+1,y1+1,x2-2,y2-2,argument[4],argument[4],argument[4],argument[4],true)
        draw_point_colour(x1,y1,argument[4]);
        draw_point_colour(x2-1,y1,argument[4]);
        draw_point_colour(x1,y2-1,argument[4]);
        draw_point_colour(x2-1,y2-1,argument[4]);
    }
}


