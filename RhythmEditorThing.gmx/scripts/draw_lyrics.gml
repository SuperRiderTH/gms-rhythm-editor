var x1,y1,x2,y2;
y1 = objRhythmEditor.surface_video_height+2+objRhythmEditor.timeline_height[1]+2;
y2 = objRhythmEditor.surface_video_height+objRhythmEditor.timeline_height[2]-2;

if instance_exists(objLyric) {
    with(all) {
        if object_index == objLyric {
        
            x1 = x+objRhythmEditor.timeline_offset;
            x2 = x1+length;
            
            draw_box(x1,y1,x2,y2,c_fuchsia);
            draw_box(x1,y1,x2+1,y2+1,c_ltgray,true);
            draw_text_ext(x1,y1,lyric,11,length);
        }
    }
}

