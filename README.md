# GMS Rhythm Editor

An as-is GameMaker: Studio concept for a Project Diva-like editor, with a working implementation of the Project Diva HUD and font.

This is a project created for fun, and has not been modified aside from some minor changes since mid 2017.

There is a lot of half working, half implemented code, and it is more of a proof of concept more than anything.